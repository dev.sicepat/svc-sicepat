const { response } = require('../../libs/http');

module.exports = (router) => {
  router.get('/', (_, res) => response(res, []));
};
