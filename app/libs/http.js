const { isError } = require('./common');

const httpStatus = {
  OK: 200,
  CREATED: 201,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  CONFLICT: 409,
  INTERNAL_SERVER_ERROR: 500,
  NOT_IMPLEMENTED: 501
};

exports.httpStatus = httpStatus;

exports.response = (res, payload, isPost = false) => {
  let status = isPost ? httpStatus.CREATED : httpStatus.OK;
  let success = true;
  const responsePayload = {};

  if (isError(payload)) {
    status = payload.status || httpStatus.INTERNAL_SERVER_ERROR;
    success = false;
  }

  responsePayload.status = status;
  responsePayload.success = success;

  if (typeof payload === 'string') {
    responsePayload.message = payload;
  } else if (typeof payload === 'object' && payload !== null) {
    responsePayload.data = payload;
  } else {
    throw new Error('payload type need to be string or object');
  }

  return res.json(responsePayload);
};
