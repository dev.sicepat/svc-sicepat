require('dotenv').config();
const express = require('express');
const enrouten = require('express-enrouten');

const app = express();

app.use(enrouten({ directory: 'controllers' }));

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
